# README #
У веб-сервиса два метода
1. получить по id данные о продукте GET api/v1/product/{id}
2. добавить новый продукт POST api/v1/products

Хранилище списка продуктов разработчик выбирает сам (файл, localdb, итд)
Путь к хранилищу указывается в appsettings.json и может быть изменен без перезапуска сервиса

Модель данных о продукте:
long Id – от 1 до long.MaxValue, автоинкремент, генерируется при добавлении нового продукта
string Name – не пустая строка, не более 200 символов
string Description – опциональное поле, не более 500 символов

Добавление продуктов должно быть доступно только авторизованным пользователям, получение продукта доступно всем
Авторизация осуществляется на основе заголовка Authorization. Схема ClientId, значение long от 1 до long.MaxValue, например Authorization: ClientId 123
Доступ разрешен только пользователям с нечетными номерами

Добавление продукта производится через POST на адрес /products
К запросу добавляется заголовок Content-Type: text/product
Тело запроса имеет вид:

Name=Страховой коробочный продукт~Description=Продукт для продаж через клиентские центры

В дополнении к стандартной валидации передаваемых полей при заведении нового продукта, на сервисе предусмотрены два интерфейса

public interface INameValidator
{
Task<bool> ValidateAsync(string name);
}

public interface IDescriptionValidator
{
Task<bool> ValidateAsync(string description);
}

Можно зарегистрировать несколько реализаций этих интерфейсов
Добавление продукта должно происходить только если все реализации вернули true. Иначе должен возвращаться statusCode: 400


Ответ сервиса всегда должен быть в виде json, с кодом 200:

200 OK
{
// реальный http код ответа, обязательное поле
"statusCode": int,

// произвольное сообщение, может содержать текст ошибки
"message": string,

// запрошенные данные, например продукт
"data": object
}


Примеры запросов и ответов

Request:
POST https://localhost:7443/api/v1/products HTTP/1.1
Authorization: ClientId 111
Content-Type: text/product;charset=UTF-8
Content-Length: 151

Name=Страховой коробочный продукт~Description=Продукт для продаж через клиентские центры

Response:
200 OK
{
"statusCode": 201,
"data": {
"id": 14,
"name": "Страховой коробочный продукт",
"description": "Продукт для продаж через клиентские центры"
}
}



Request:
GET https://localhost:7443/api/v1/product/14 HTTP/1.1

Response:
200 OK
{
"statusCode": 200,
"data": {
"id": 14,
"name": "Страховой коробочный продукт",
"description": "Продукт для продаж через клиентские центры"
}
}



Request:
POST https://localhost:7443/api/v1/products HTTP/1.1
Authorization: ClientId 111
Content-Type: text/product;charset=UTF-8
Content-Length: 97

Name=~Description=Продукт для продаж через клиентские центры

Response:
200 OK
{
"statusCode": 400,
"message": "Не указано название продукта"
}