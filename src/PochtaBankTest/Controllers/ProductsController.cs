﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PochtaBankTest.Domain.Services.Abstractions;
using PochtaBankTest.Models;

namespace PochtaBankTest.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [Authorize(Policy = "OnlyEven")]
        [HttpPost]
        public async Task<IActionResult> CreateProduct([FromBody]CreateProductModel model)
        {
            var result = await _productService.Create(model);

            return StatusCode(201, result);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetProduct(long id)
        {
            var result = await _productService.Get(id);
            return Ok(result);
        }
    }
}
