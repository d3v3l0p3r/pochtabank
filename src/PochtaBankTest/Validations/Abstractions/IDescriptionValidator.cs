﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PochtaBankTest.Validations.Abstractions
{
    public interface IDescriptionValidator
    {
        Task<bool> ValidateAsync(string description);
    }
}
