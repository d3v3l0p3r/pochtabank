﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PochtaBankTest.Validations.Abstractions
{
    public interface INameValidator
    {
        Task<bool> ValidateAsync(string name);
    }
}
