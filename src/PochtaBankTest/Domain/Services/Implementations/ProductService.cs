﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PochtaBankTest.DAL;
using PochtaBankTest.DAL.Entities;
using PochtaBankTest.Domain.Services.Abstractions;
using PochtaBankTest.Models;

namespace PochtaBankTest.Domain.Services.Implementations
{
    public class ProductService : IProductService
    {
        private readonly IRepository _repository;

        public ProductService(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<ProductDto> Create(CreateProductModel model)
        {
            var entity = new Product
            {
                Name = model.Name,
                Description = model.Description
            };

            await _repository.Create(entity);

            return (ProductDto) entity;
        }

        public async Task<ProductDto> Get(long id)
        {
            var entity = await _repository.Get<Product>(id);
            if (entity == null)
            {
                throw new Exception("Сущность не найдена");
            }

            var dto = new ProductDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description
            };

            return dto;
        }

    }
}
