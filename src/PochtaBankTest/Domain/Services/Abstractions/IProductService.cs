﻿using System.Threading.Tasks;
using PochtaBankTest.Models;

namespace PochtaBankTest.Domain.Services.Abstractions
{
    public interface IProductService
    {
        Task<ProductDto> Create(CreateProductModel model);
        Task<ProductDto> Get(long id);
    }
}