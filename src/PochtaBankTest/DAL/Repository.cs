﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PochtaBankTest.DAL.Entities;

namespace PochtaBankTest.DAL
{
    public class Repository : IRepository
    {
        private readonly DataContext _context;

        public Repository(DataContext context)
        {
            _context = context;
        }

        public async Task Create<T>(T entity) where T : Entity
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public Task<T> Get<T>(long id) where T : Entity
        {
            return _context.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

    }
}
