﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PochtaBankTest.DAL.Entities
{
    public abstract class Entity
    {
        public long Id { get; set; }
    }
}
