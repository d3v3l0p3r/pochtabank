﻿using System.Threading.Tasks;
using PochtaBankTest.DAL.Entities;

namespace PochtaBankTest.DAL
{
    public interface IRepository
    {
        Task Create<T>(T entity) where T : Entity;
        Task<T> Get<T>(long id) where T : Entity;
    }
}