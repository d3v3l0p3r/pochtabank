﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using PochtaBankTest.Models;

namespace PochtaBankTest.Middlewares
{
    public class TextMediaFormatter : TextInputFormatter
    {
        private const string NameProperty = "NAME";
        private const string DescriptionProperty = "DESCRIPTION";

        public TextMediaFormatter()
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("text/product"));
            SupportedEncodings.Add(Encoding.UTF8);
        }

        public override async Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context, Encoding encoding)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            var request = context.HttpContext.Request;

            using (var reader = new StreamReader(request.Body, encoding))
            {
                try
                {
                    var bodyString = await reader.ReadLineAsync();
                    var props = bodyString.Split('~').ToDictionary(x=> x.Split('=')[0].ToUpper(), x=>x.Split('=')[1]?.ToUpper());

                    var productCreateModel = new CreateProductModel
                    {
                        Name = props[NameProperty],
                        Description = props[DescriptionProperty]
                    };

                    return await InputFormatterResult.SuccessAsync(productCreateModel);
                }
                catch
                {
                    return await InputFormatterResult.FailureAsync();
                }
            }
        }
    }
}
