﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;

namespace PochtaBankTest.Middlewares
{
    public class OnlyEvenPolicy : AuthorizationPolicy
    {
        public OnlyEvenPolicy(IEnumerable<IAuthorizationRequirement> requirements, IEnumerable<string> authenticationSchemes) : base(requirements, authenticationSchemes)
        {
        }
    }

    public class EvenRequire : IAuthorizationRequirement
    {
        public bool IsEven(long value)
        {
            return value % 2 == 0;
        }
    }

    public class EvenAuthorizationHandler : AuthorizationHandler<EvenRequire>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, EvenRequire requirement)
        {
            var id = long.Parse(context.User.Identity.Name);

            if (!requirement.IsEven(id)) //если нечетное то доступ есть 
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }

            return Task.CompletedTask;
        }
    }
}
