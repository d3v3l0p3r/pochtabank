﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace PochtaBankTest.Middlewares
{
    public class BasicAuthenticationOptions : AuthenticationSchemeOptions
    {
    }

    public class AuthenticationHandler : AuthenticationHandler<BasicAuthenticationOptions>
    {
        private const string AuthHeaderName = "Authorization";
        private const string AuthHeaderValue = "CLIENTID";

        public AuthenticationHandler(IOptionsMonitor<BasicAuthenticationOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey(AuthHeaderName))
            {
                return AuthenticateResult.Fail("Unauthorized");
            }

            string authHeader = Request.Headers[AuthHeaderName];
            if (string.IsNullOrEmpty(authHeader))
            {
                return AuthenticateResult.NoResult();
            }

            authHeader = authHeader.ToUpper();

            if (!authHeader.ToUpper().StartsWith(AuthHeaderValue))
            {
                return AuthenticateResult.Fail("Unauthorized");
            }

            var headerValue = authHeader.Replace(AuthHeaderValue, "").Trim();

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, headerValue)
            };

            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new GenericPrincipal(identity, null);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);
            return AuthenticateResult.Success(ticket);
        }
    }

}
