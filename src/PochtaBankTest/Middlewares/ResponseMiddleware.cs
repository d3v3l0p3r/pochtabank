﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using PochtaBankTest.Models;
using JsonConverter = System.Text.Json.Serialization.JsonConverter;

namespace PochtaBankTest.Middlewares
{
    public class ResponseMiddleware
    {
        private readonly RequestDelegate _next;

        public ResponseMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var originalBody = context.Response.Body;
            using (var ms = new MemoryStream())
            {
                context.Response.Body = ms;

                await _next(context);

                var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();

                var result = new ResponseModel()
                {
                    StatusCode = context.Response.StatusCode,
                    Message = exceptionHandlerPathFeature?.Error?.Message,
                    Data = await ReadJsonBody(context.Response)
                };

                context.Response.Body = originalBody;

                await context.Response.WriteAsync(JsonConvert.SerializeObject(result), Encoding.UTF8);
            }
        }

        private async Task<object> ReadJsonBody(HttpResponse response)
        {
            var buffer = new byte[response.Body.Length];

            response.Body.Seek(0, SeekOrigin.Begin);
            await response.Body.ReadAsync(buffer);

            var stringBody = Encoding.UTF8.GetString(buffer);

            var bodyObject = JsonConvert.DeserializeObject(stringBody);

            return bodyObject;
        }


    }
}
