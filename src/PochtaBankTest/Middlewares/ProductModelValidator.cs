﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.EntityFrameworkCore.Internal;
using PochtaBankTest.DAL.Entities;
using PochtaBankTest.Models;
using PochtaBankTest.Validations.Abstractions;


namespace PochtaBankTest.Middlewares
{
    public class ProductModelValidator : IAsyncActionFilter
    {
        private readonly IList<INameValidator> _nameValidators;
        private readonly IList<IDescriptionValidator> _descriptionValidators;

        public ProductModelValidator(IList<INameValidator> nameValidators, IList<IDescriptionValidator> descriptionValidators)
        {
            _nameValidators = nameValidators;
            _descriptionValidators = descriptionValidators;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var errors = new List<ModelValidationResult>();
            var model = context.ActionArguments.Values.FirstOrDefault(x => x is CreateProductModel) as CreateProductModel;
            if (model == null)
            {
                await next();
                return;
            }

            foreach (var nameValidator in _nameValidators)
            {
                var result = await nameValidator.ValidateAsync(model.Name);
                if (!result)
                {
                    throw new Exception("Model state is invalid");
                }
            }

            foreach (var descriptionValidator in _descriptionValidators)
            {
                var result = await descriptionValidator.ValidateAsync(model.Description);
                if (!result)
                {
                    throw new Exception("Model state is invalid");
                }
            }

            await next();
        }
    }
}
