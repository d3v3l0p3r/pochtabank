using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PochtaBankTest.DAL;
using PochtaBankTest.Domain.Services.Abstractions;
using PochtaBankTest.Domain.Services.Implementations;
using PochtaBankTest.Middlewares;
using PochtaBankTest.Validations.Abstractions;

namespace PochtaBankTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(cfg =>
            {
                cfg.InputFormatters.Insert(0, new TextMediaFormatter());
                cfg.Filters.Add<ProductModelValidator>();
            });

            services.AddDbContext<DataContext>(cfg =>
            {
                var dbName = Configuration.GetValue<string>("DataBaseName");
                cfg.UseInMemoryDatabase(dbName);
            });


            services.AddAuthentication("ClientId")
                .AddScheme<BasicAuthenticationOptions, AuthenticationHandler>("ClientId", null);

            services.AddAuthorization(cfg =>
            {
                cfg.AddPolicy("OnlyEven", c =>
                {
                    c.Requirements.Add(new EvenRequire());
                });
            });
            services.AddSingleton<IAuthorizationHandler, EvenAuthorizationHandler>();


            services.AddTransient<IRepository, Repository>();
            services.AddTransient<IProductService, ProductService>();

            services.AddTransient<IList<INameValidator>>(x => x.GetServices<INameValidator>().ToList());
            services.AddTransient<IList<IDescriptionValidator>>(x=> x.GetServices<IDescriptionValidator>().ToList());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ResponseMiddleware>();
            app.UseMiddleware<ExceptionMiddleware>();


            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
